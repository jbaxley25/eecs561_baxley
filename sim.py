#!/usr/bin/env python3
# Final Project for EECS 561
# Jack Baxley jbaxley@umich.edu
# Spring 2020

import math
import numpy as np
import matplotlib.pyplot as plt

class Profile:
	def __init__(self, ambient_temp  = 25,
					   ramp1_slope   = 2,
					   preheat_temp  = 150,
					   ramp2_slope   = 2,
					   reflow_temp   = 220,
					   cool_slope    = -3,
					   flat_dur      = 30,
					   preheat_dur   = 90,
					   reflow_dur    = 60,
					   min_eval_temp      = 100,
					   slope_tol          = 1,
					   preheat_temp_tol   = 20,
					   preheat_dur_tol    = 30,
					   reflow_temp_min    = 217,
					   reflow_dur_tol     = 15
			):

		self.flat_temp = ambient_temp
		self.ramp1_slope = ramp1_slope
		self.preheat_temp = preheat_temp
		self.ramp2_slope = ramp2_slope
		self.reflow_temp = reflow_temp
		self.cool_slope = cool_slope

		self.flat_dur = flat_dur
		self.ramp1_dur = (preheat_temp - ambient_temp) / ramp1_slope
		self.preheat_dur = preheat_dur
		self.ramp2_dur = (reflow_temp - preheat_temp) / ramp2_slope
		self.reflow_dur = reflow_dur
		self.cool_dur = (ambient_temp -  reflow_temp) / cool_slope

		self.ramp1_start = self.flat_dur
		self.preheat_start = self.ramp1_start + self.ramp1_dur
		self.ramp2_start = self.preheat_start + self.preheat_dur
		self.reflow_start = self.ramp2_start + self.ramp2_dur
		self.cool_start = self.reflow_start + self.reflow_dur
		self.end = self.cool_start + self.cool_dur

		self.min_eval_temp = min_eval_temp
		self.slope_tol = slope_tol
		self.preheat_temp_tol = preheat_temp_tol
		self.preheat_dur_tol = preheat_dur_tol
		self.reflow_temp_min = reflow_temp_min
		self.reflow_dur_tol = reflow_dur_tol
		

	def temp_at(self, t):

		if t < self.ramp1_start:
			return self.flat_temp
		if t < self.preheat_start:
			return self.flat_temp + (t - self.ramp1_start) * self.ramp1_slope
		if t < self.ramp2_start:
			return self.preheat_temp
		if t < self.reflow_start:
			return self.preheat_temp + (t - self.ramp2_start) * self.ramp2_slope
		if t < self.cool_start:
			return self.reflow_temp
		if t < self.end:
			return self.reflow_temp + (t - self.cool_start) * self.cool_slope
		return self.flat_temp

	def eval(self, time, temp):
		i = 0
		
		#Get times of phase transitions
		while i < len(temp):
			if temp[i] >= self.min_eval_temp:
				break
			i+=1

		start = i
		while i < len(temp):
			if temp[i] >= self.preheat_temp - self.preheat_temp_tol/2:
				break
			i+=1
		preheat_start = i
		
		while i < len(temp):
			if temp[i] >= self.preheat_temp + self.preheat_temp_tol:
				break
			i+=1
		preheat_end = i

		while i < len(temp):
			if temp[i] >= ( self.reflow_temp + self.reflow_temp_min ) / 2:
				break
			i+=1
		reflow_start = i

		while i < len(temp):
			if temp[i] <= self.reflow_temp_min:
				break
			i+=1
		reflow_end = i

		while i < len(temp):
			if temp[i] <= self.min_eval_temp:
				break
			i+=1
		end = i

		if True:
			print("start         : ", start)
			print("preheat_start : ", preheat_start)
			print("preheat_end   : ", preheat_end)
			print("reflow_start  : ", reflow_start)
			print("reflow_end    : ", reflow_end)
			print("end           : ", end)
		
		#Evaluate contraints

		error_msg = ""
		passed = True
		error = 0


		if preheat_start-start == 0:
			passed = False
			error_msg = error_msg + "ramp1 too quick "

		if reflow_start-preheat_end == 0:
			passed = False
			error_msg = error_msg + "ramp2 too quick "

		if end - reflow_end == 0:
			passed = False
			error_msg = error_msg + "ramp3 too quick "

		if not passed:
			return passed, error_msg

		preheat_start_time = time[preheat_start] + 0.5 * self.preheat_temp_tol / self.ramp1_slope
		preheat_end_time = time[preheat_end] - 1.0 * self.preheat_temp_tol / self.ramp1_slope

		reflow_start_time = time[reflow_start] + (self.reflow_temp - self.reflow_temp_min) / self.ramp2_slope
		reflow_end_time = time[reflow_end] 


		slope1 = (temp[preheat_start]-temp[start]) / (time[preheat_start]-time[start])
		slope2 = (temp[reflow_start]-temp[preheat_end]) / (time[reflow_start]-time[preheat_end])
		slope3 = (temp[end]-temp[reflow_end]) / (time[end]-time[reflow_end])


		preheat_dur = preheat_end_time - preheat_start_time
		reflow_dur  = reflow_end_time - reflow_start_time

		preheat_in_bounds = True
		for i in range(preheat_start, preheat_end):
			if time[i] > preheat_start_time and time[i] < preheat_end_time:
				err = abs(temp[i] - self.preheat_temp) - self.preheat_temp_tol
				if err > 0:
					error += err*err
					preheat_in_bounds = False

		if not preheat_in_bounds:
			passed = False
			error_msg = error_msg + "Preheat temp out of bounds "

		reflow_in_bounds = True
		for i in range(reflow_start, reflow_end):
			if time[i] > reflow_start_time and time[i] < reflow_end_time:
				err = self.reflow_temp_min - temp[i]
				if err > 0:
					error += err*err
					reflow_in_bounds = False

		if not reflow_in_bounds:
			passed = False
			error_msg = error_msg + "Reflow temp out of bounds "

		if abs(slope1-self.ramp1_slope) > self.slope_tol:
			passed = False
			error_msg = error_msg + "ramp1 slope out of tolorence "

		if abs(preheat_dur - self.preheat_dur) > self.preheat_dur_tol:
			passed = False
			error_msg = error_msg + "preheat duration out of tolorence "

		if abs(slope2-self.ramp2_slope) > self.slope_tol:
			passed = False
			error_msg = error_msg + "ramp2 slope out of tolorence "

		if abs(reflow_dur - self.reflow_dur) > self.reflow_dur_tol:
			passed = False
			error_msg = error_msg + "reflow duration out of tolorence "

		if abs(slope3-self.cool_slope) > self.slope_tol:
			passed = False
			error_msg = error_msg + "cool slope out of tolorence "


		if True:
			print("Ramp1 Slope      : ", slope1)
			print("Ramp2 Slope      : ", slope2)
			print("Cool Slope       : ", slope3)
			print("Preheat Duration : ", preheat_dur)
			print("Reflow_Duration  : ", reflow_dur)
			print("Error            : ", error)



		if passed:
			error_msg = "Passed!"

		return passed, error_msg







class PIDController:
	
	def __init__(self, kp = 1, kd = 0, ki = 0):
		self.kp = kp
		self.kd = kd
		self.ki = ki
		self.reset()
	
	def reset(self):
		self.int = 0
		self.last_error = 0
		self.first_step = True
	
	def step(self,error):
		self.int += error
		der = error - self.last_error
		if self.first_step:
			der = 0
			self.first_step = False
		self.last_error = error
		return self.kp * error + self.kd * der + self.ki * self.int

class OvenModel:
	def __init__(self, ambient_temp = 25, tau = 40, max_temp = 350, sim_T = .01, air_mass = 1, elem_mass = 1, single_temp = False):
		self.air_loss_coeff = 1 - math.exp(-sim_T / tau)

		self.xfer_coeff = 1 * self.air_loss_coeff

		self.elem_heat_coeff = self.air_loss_coeff * (max_temp - ambient_temp) * air_mass  / elem_mass

		self.air_mass = air_mass
		self.elem_mass = elem_mass
		self.ambient_temp = ambient_temp
		
		self.single_temp = single_temp

		self.reset()
	
	def reset(self):
		self.air_temp = self.ambient_temp
		self.elem_temp = self.ambient_temp 

	def step(self,cont):
		old_air_temp = self.air_temp
		old_elem_temp = self.elem_temp
	
		if not self.single_temp:
			heat_xfer = ( old_elem_temp - old_air_temp ) * self.xfer_coeff

			new_air_temp  = old_air_temp - self.air_loss_coeff * (old_air_temp - self.ambient_temp) + heat_xfer / self.air_mass 
			new_elem_temp = old_elem_temp - heat_xfer / self.elem_mass + self.elem_heat_coeff * cont 

		else:
			new_air_temp = old_air_temp - self.air_loss_coeff * (old_air_temp - self.ambient_temp) + self.elem_heat_coeff * cont * self.elem_mass / self.air_mass
			new_elem_temp = new_air_temp

		self.air_temp = new_air_temp
		self.elem_temp = new_elem_temp

		return old_air_temp, old_elem_temp

		
		
		
	
def run_simulation(sim_length, sim_T, cont_T, model, controller, profile):

	model.reset()
	controller.reset()

	n_steps = math.floor(sim_length/sim_T)

	time_hist = np.zeros(n_steps)
	air_temp_hist = np.zeros(n_steps)
	elem_temp_hist = np.zeros(n_steps)
	cont_hist = np.zeros(n_steps)
	targ_hist = np.zeros(n_steps)

	old_k = -1
	cont_out = 0
	targ = 0
	
	for i in range( n_steps ):
		t = i * sim_T

		# Simulation Calculations
		air_temp, elem_temp = model.step( cont_out ) 

		#Controller Calculations
		k = math.floor(t / cont_T)
		if k != old_k:
			#Controller Tick
			#Calc Target Temp
			targ = 	profile.temp_at(t)
			
			#Calculate Err, profiler
			error = targ - air_temp
			#Calculate Controller Output
			cont_out = controller.step(error)

			#Clamp output to [0,1]
			cont_out = max(min(cont_out, 1), 0)

		time_hist[i] = t
		air_temp_hist[i] = air_temp
		elem_temp_hist[i] = elem_temp
		cont_hist[i] = cont_out
		targ_hist[i] = targ

		old_k = k

	return time_hist, air_temp_hist, elem_temp_hist, cont_hist, targ_hist
		
#Ambient Temperature (C)
ambient_temp = 25
#Maximum temperature (C) if controller output is stuck at one
max_temp = 500
#Time constant of oven cooling down (s)
tau = 20
#Simulation time step (s)
sim_T = .01
#simulation run time (s)
runtime = 500
#Controller time step (s)
cont_T = .1

model = OvenModel(ambient_temp = ambient_temp, tau = tau, max_temp = max_temp, sim_T = sim_T, air_mass = 1, single_temp = False)
controller = PIDController(kp = 1, ki = .000, kd = -0)
profile = Profile(ambient_temp = ambient_temp)


time, air_temp, elem_temp, cont, targ = run_simulation(runtime, sim_T, cont_T, model, controller, profile)

passed, msg = profile.eval(time,air_temp)
print(msg)


fig, ax_list = plt.subplots(2,2)

ax_list[0,0].plot(time, air_temp, label = "Air Temperature")
ax_list[0,0].plot(time, elem_temp, label = "Element Temperature")
ax_list[0,0].plot(time, targ, label = "Target Temperature")
ax_list[0,0].legend()
ax_list[0,0].set_title("Temperature vs Time")
ax_list[0,0].set_xlabel("Time (s)")
ax_list[0,0].set_ylabel("Temp (C)")

ax_list[0,1].plot(time, cont)
ax_list[0,1].set_title("Controller Output")
ax_list[0,1].set_xlabel("Time (s)")

ax_list[1,0].plot(time, targ)
ax_list[1,0].set_title("Target Temperature")
ax_list[1,0].set_xlabel("Time (s)")
ax_list[1,0].set_ylabel("Temp (C)")

fig.tight_layout(pad=0.2)
plt.show()

'''
ambient_temp = 25
max_temp = 500
tau = 20
sim_T = .01
runtime = 500
cont_T = .1

model = OvenModel(ambient_temp = ambient_temp, tau = tau, max_temp = max_temp, sim_T = sim_T, air_mass = 1, single_temp = False)
controller0 = PIDController(kp = .01, ki = .000, kd = -0)
controller1 = PIDController(kp = .1, ki = .000, kd = -0)
controller2 = PIDController(kp = 1, ki = .000, kd = -0)
profile = Profile(ambient_temp = ambient_temp)


time0, air_temp0, elem_temp0, cont0, targ0 = run_simulation(runtime, sim_T, cont_T, model, controller0, profile)
time1, air_temp1, elem_temp1, cont1, targ1 = run_simulation(runtime, sim_T, cont_T, model, controller1, profile)
time2, air_temp2, elem_temp2, cont2, targ2 = run_simulation(runtime, sim_T, cont_T, model, controller2, profile)

#passed, msg = profile.eval(time,air_temp)
#print(msg)
fig, ax_list = plt.subplots(3,1)

ax_list[0].plot(time0, air_temp0, label = "Air Temperature")
ax_list[0].plot(time0, elem_temp0, label = "Element Temperature")
ax_list[0].plot(time0, targ0, label = "Target Temperature")
ax_list[0].legend()
ax_list[0].set_title("Kp = .01")
ax_list[0].set_xlabel("Time (s)")
ax_list[0].set_ylabel("Temp (C)")

ax_list[1].plot(time1, air_temp1, label = "Air Temperature")
ax_list[1].plot(time1, elem_temp1, label = "Element Temperature")
ax_list[1].plot(time1, targ1, label = "Target Temperature")
ax_list[1].legend()
ax_list[1].set_title("Kp = .1")
ax_list[1].set_xlabel("Time (s)")
ax_list[1].set_ylabel("Temp (C)")

ax_list[2].plot(time2, air_temp2, label = "Air Temperature")
ax_list[2].plot(time2, elem_temp2, label = "Element Temperature")
ax_list[2].plot(time2, targ2, label = "Target Temperature")
ax_list[2].legend()
ax_list[2].set_title("Kp = 1")
ax_list[2].set_xlabel("Time (s)")
ax_list[2].set_ylabel("Temp (C)")

fig.tight_layout(pad=0.2)


plt.show()

'''
